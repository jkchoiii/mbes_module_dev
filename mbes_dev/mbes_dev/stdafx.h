#pragma once

#include <vector>
#include <iostream>
#include <chrono>

#define _USE_MATH_DEFINES
#include <math.h>

#define EIGEN_MPL2_ONLY
#define EIGEN_USE_MKL_ALL
#define EIGEN_FFTW_DEFAULT
#include <Eigen/Core>
#include <unsupported/Eigen/FFT>


#include "binaryio.hpp"
#include "adpcm.h"