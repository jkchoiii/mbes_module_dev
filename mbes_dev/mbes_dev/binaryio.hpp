#pragma once
#include <fstream>

template<class T>
void BinaryReader(std::vector<T>& data, std::string filename)
{
	std::streampos size;
	std::ifstream file(filename, std::ios::binary | std::ios::in | std::ios::ate);

	if(file.is_open())
	{
		size = file.tellg();

		//TRACE(_T("size=%d\n", size));

		char* memblock = new char[size];	
		file.seekg(0, std::ios::beg);
		file.read(memblock, size);
		file.close();

		//TRACE(_T("the entire file content is in memory \n"));

		T* T_values = reinterpret_cast<T*>(memblock);

		data.resize(size / sizeof(T));
		data.assign(T_values, T_values + size / sizeof(T));

		delete[] memblock;
		
	}
}

template<class T>
void BinaryReader(T* data, std::string filename, int align = 64)
{
	std::streampos size;
	std::ifstream file(filename, std::ios::binary | std::ios::in | std::ios::ate);

	if (file.is_open())
	{
		size = file.tellg();

		//TRACE(_T("size=%d\n", size));

		char* memblock = new char[size];
		file.seekg(0, std::ios::beg);
		file.read(memblock, size);
		file.close();

		//TRACE(_T("the entire file content is in memory \n"));

		T* T_values = reinterpret_cast<T*>(memblock);

		data = (T*)_aligned_malloc(size, align);
		std::copy(T_values, T_values + size / sizeof(T), data);

		delete[] memblock;

	}
}


template<class T>
void BinaryWriter(const std::vector<T>& data, std::string filename)
{
	std::ofstream fout;
	fout.open(filename, std::ios::out | std::ios::binary);
	if(fout.is_open())
	{
		fout.write((const char*)data.data(), sizeof(T)*data.size());
		fout.close();
	}
}

template<class T>
void BinaryWriter(const T* data, int data_size, std::string filename)
{	
	std::ofstream fout;
	fout.open(filename, std::ios::out | std::ios::binary);
	if (fout.is_open())
	{
		fout.write((const char*)data, sizeof(T)*data_size);
		fout.close();
	}
}
