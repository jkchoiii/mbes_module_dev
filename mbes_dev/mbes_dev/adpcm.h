#pragma once
#include <numeric>
#include <vector>
#include <omp.h>

struct Decoded
{
    int16_t* dst_head;
    uint16_t* dst_tail;
    uint8_t* bit;

};

constexpr unsigned short step_table[89] = {
    7, 8, 9, 10, 11, 12, 13, 14,
    16, 17, 19, 21, 23, 25, 28, 31,
    34, 37, 41, 45, 50, 55, 60, 66,
    73, 80, 88, 97, 107, 118, 130, 143,
    157, 173, 190, 209, 230, 253, 279, 307,
    337, 371, 408, 449, 494, 544, 598, 658,
    724, 796, 876, 963, 1060, 1166, 1282, 1411,
    1552, 1707, 1878, 2066, 2272, 2499, 2749, 3024,
    3327, 3660, 4026, 4428, 4871, 5358, 5894, 6484,
    7132, 7845, 8630, 9493, 10442, 11487, 12635, 13899,
    15289, 16818, 18500, 20350, 22385, 24623, 27086, 29794,
    32767
};

constexpr int index_table[] = {
    -1, -1, -1, -1, 2, 4, 6, 8
};

enum CompressBit
{
    Bit4, Bit7
};

class ADPCM
{
    int m_max_beam_count, m_max_data_count;
    int beam_count, data_count;
    int data_size;

    struct CompressInfo
    {
        std::vector<uint8_t> prev_idx;
        std::vector<int16_t> prev_sample;
    };

    std::vector<uint8_t> m_data_slice;
    std::vector<uint64_t> m_data_idx;

    CompressInfo m_tail_info, m_head_info; // head: phase, tail: mag

    

private:

    void Init(int beam_count);
    void SetZeros();

    int32_t Clip(int32_t data, int32_t min, int32_t max);
    

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // 4bit 
    void Decode4BitCompressed(
        int beam_count,
        int data_count,
        uint8_t* __restrict src_data,
        Decoded* __restrict dst_decode);

    // signed/unsigned
    void DecodeWorkers4Bit(
        int idx,
        uint8_t* __restrict src_data,
        uint64_t* __restrict data_idx,
        CompressInfo* __restrict m_tail_info,
        CompressInfo* __restrict m_head_info,
        uint16_t* __restrict dst_tail,
        int16_t* __restrict dst_head);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // 7bit
    void Decode7BitCompressed(
        int beam_count, 
        int data_count, 
        uint8_t* __restrict src_data, 
        Decoded* __restrict dst_decode,
        int num_core);

    // signed 7bit
    uint8_t DecodeWorkers7Bit(
        int idx,
        uint8_t* __restrict src_data,
        uint64_t* __restrict data_idx,
        CompressInfo* __restrict m_info,
        int16_t* __restrict dst);

    // unsigned 7bit
    uint8_t DecodeWorkers7Bit(
        int idx,
        uint8_t* __restrict src_data,
        uint64_t* __restrict data_idx,
        CompressInfo* __restrict m_info,
        uint16_t* __restrict dst);

    // bit extraction
    void FillBitFlags(int i, int j, int beam_count, uint8_t head_bit, uint8_t tail_bit, uint8_t* dst_bit);


public:
    
    ADPCM(int max_beam_count, int max_data_count);
    
    void DecodeCompressedBlock(
        int beam_count, 
        int data_count, 
        CompressBit compr_bit,
        uint8_t* __restrict src_data, 
        Decoded* __restrict dst_decode,
        int num_core);
    
};

inline ADPCM::ADPCM(int max_beam_count, int max_data_count)
{
    this->m_max_beam_count = max_beam_count;
    this->m_max_data_count = max_data_count;

    // allocation

    m_tail_info.prev_idx.reserve(max_beam_count);
    m_tail_info.prev_sample.reserve(max_beam_count);

    m_head_info.prev_idx.reserve(max_beam_count);
    m_head_info.prev_sample.reserve(max_beam_count);

    m_data_idx.reserve(max_beam_count);
    m_data_slice.reserve(max_beam_count);
}

inline uint8_t ADPCM::DecodeWorkers7Bit(
    int idx, 
    uint8_t* __restrict src_data,
    uint64_t* __restrict data_idx,
    CompressInfo* __restrict m_info,
    uint16_t* __restrict dst)
{
    //////////////////////////////
    // magnitude
    //////////////////////////////
    
    int32_t predsample = m_info->prev_sample[idx];
    long index = m_info->prev_idx[idx];

    int step = step_table[index];
    
    uint8_t code = src_data[idx] & 0x7F;

    int delta = step >> 6;

    if (code & 32) delta += step;
    if (code & 16) delta += (step >> 1);
    if (code & 8) delta += (step >> 2);
    if (code & 4) delta += (step >> 3);
    if (code & 2) delta += (step >> 4);
    if (code & 1) delta += (step >> 5);

    if (code & 64)
        predsample -= delta;
    else
        predsample += delta;

    index += index_table[code & 0x7];

    index = Clip(index, 0, 88);
    predsample = Clip(predsample, 0, 65535);

    dst[data_idx[idx] / 2] = static_cast<uint16_t>(predsample);
    m_info->prev_sample[idx] = static_cast<int16_t>(predsample);
    m_info->prev_idx[idx] = index;
    return (src_data[idx] & 0x80) >> 7;
}

inline uint8_t ADPCM::DecodeWorkers7Bit(
    int idx, 
    uint8_t* __restrict src_data,
    uint64_t* __restrict data_idx,
    CompressInfo* __restrict m_info,
    int16_t* __restrict dst)
{
    //////////////////////////////
    // phase
    //////////////////////////////
    
    int32_t predsample = m_info->prev_sample[idx];
    long index = m_info->prev_idx[idx];

    int step = step_table[index];

    uint8_t code = src_data[idx] & 0x7F;

    int delta = step >> 6;

    if (code & 32) delta += step;
    if (code & 16) delta += (step >> 1);
    if (code & 8) delta += (step >> 2);
    if (code & 4) delta += (step >> 3);
    if (code & 2) delta += (step >> 4);
    if (code & 1) delta += (step >> 5);

    if (code & 64) 
        predsample -= delta;
    else 
        predsample += delta;

    index += index_table[code & 0x7];

    index = Clip(index, 0, 88);
    predsample = Clip(predsample, -32768, 32767);

    dst[(data_idx[idx] - 1) / 2] = static_cast<int16_t>(predsample);
    m_info->prev_sample[idx] = static_cast<int16_t>(predsample);
    m_info->prev_idx[idx] = index;

    return (src_data[idx] & 0x80) >> 7;
}

inline void ADPCM::DecodeWorkers4Bit(
    int idx, 
    uint8_t* __restrict src_data, 
    uint64_t* __restrict data_idx, 
    CompressInfo* __restrict m_mag_info,
    CompressInfo* __restrict m_phase_info,
    uint16_t* __restrict dst_tail, 
    int16_t* __restrict dst_head)
{

    /*****************************************************************
     * head: [0-3]bit (phases)
     *****************************************************************/

    int32_t predsample = m_phase_info->prev_sample[idx];
    long index = m_phase_info->prev_idx[idx];

    int step = step_table[index];

    uint8_t code = src_data[idx];
    int delta = step >> 3;
    if (code & 4) delta += step;
    if (code & 2) delta += (step >> 1);
    if (code & 1) delta += (step >> 2);
    if (code & 8) predsample -= delta;
    else predsample += delta;

    index += index_table[code & 0x7];

    index = Clip(index, 0, 88);
    predsample = Clip(predsample, -32768, 32767);

    dst_head[data_idx[idx]] = static_cast<int16_t>(predsample);
    m_phase_info->prev_sample[idx] = static_cast<int16_t>(predsample);
    m_phase_info->prev_idx[idx] = index;


    /*****************************************************************
     * tail: [4-7]bit (magnitudes)
     *****************************************************************/

    predsample = m_mag_info->prev_sample[idx];
    index = m_mag_info->prev_idx[idx];

    step = step_table[index];

    delta = step >> 3;
    if (code & 0x40) delta += step;
    if (code & 0x20) delta += (step >> 1);
    if (code & 0x10) delta += (step >> 2);
    if (code & 0x80) predsample -= delta;
    else predsample += delta;

    index += index_table[(code >> 4) & 0x7];

    index = Clip(index, 0, 88);
    //predsample = Clip(predsample, -32768, 32767); // signed
    predsample = Clip(predsample, 0, 65535); // unsigned
    
    dst_tail[data_idx[idx]] = static_cast<uint16_t>(Clip(predsample, 0, 65535));
    m_mag_info->prev_sample[idx] = static_cast<uint32_t>(predsample);
    m_mag_info->prev_idx[idx] = index;
}

inline int32_t ADPCM::Clip(int32_t data, int32_t min, int32_t max)
{
    if (data > max) 
        data = max;
    else if (data < min) 
        data = min;

    return data;
}

inline void ADPCM::Init(int beam_count)
{
    m_data_idx.resize(beam_count);
    m_data_slice.resize(beam_count);

    m_tail_info.prev_idx.resize(beam_count, 0);
    m_tail_info.prev_sample.resize(beam_count, 0);

    m_head_info.prev_idx.resize(beam_count, 0);
    m_head_info.prev_sample.resize(beam_count, 0);

    SetZeros();

}

inline void ADPCM::SetZeros()
{
    // 매 핑마다 초기화

    std::fill(m_head_info.prev_idx.begin(), m_head_info.prev_idx.end(), 0);
    std::fill(m_head_info.prev_sample.begin(), m_head_info.prev_sample.end(), 0);

    std::fill(m_tail_info.prev_idx.begin(), m_tail_info.prev_idx.end(), 0);
    std::fill(m_tail_info.prev_sample.begin(), m_tail_info.prev_sample.end(), 0);

}

inline void ADPCM::Decode4BitCompressed(int beam_count, int data_count, uint8_t* src_data, Decoded* dst_decode)
{
    Init(beam_count);

    uint8_t* p_data_slice = m_data_slice.data();
    uint64_t* p_data_idx = m_data_idx.data();
    CompressInfo* p_tail_info = &m_tail_info;
    CompressInfo* p_head_info = &m_head_info;
    uint16_t* dst_tail = dst_decode->dst_tail;
    int16_t* dst_head = dst_decode->dst_head;

    for (int i = 0; i < data_count; i++)
    {

        std::iota(p_data_idx, p_data_idx + beam_count, i * beam_count);
        memcpy(p_data_slice, &src_data[p_data_idx[0]], beam_count);

        #pragma omp parallel num_threads(2)
        {
            #pragma omp for
            for (int j = 0; j < beam_count; j++) // N번째 데이터 샘플에 대해 beam_count만큼 압축 해제(서로 독립적)
            {
                DecodeWorkers4Bit(j, p_data_slice, p_data_idx,
                    p_tail_info, p_head_info, dst_tail, dst_head);
            }
        }
    }
        
}

inline void ADPCM::FillBitFlags(int i, int j, int beam_count, uint8_t head_bit, uint8_t tail_bit, uint8_t* dst_bit)
{
    uint8_t fill_bit = 0;
    if (head_bit && tail_bit) // high confidence
        fill_bit = 16;
    else if (tail_bit) // mid confidence 
        fill_bit = 4;
    else if(head_bit) // low confidence
        fill_bit = 1;
        //fill_bit = 0;
        
    
    dst_bit[j / 2 + beam_count * i] = fill_bit;
    
}

inline void ADPCM::Decode7BitCompressed(int beam_count, int data_count, uint8_t* src_data, Decoded* dst_decode, int num_core)
{
    Init(2 * beam_count);

    uint8_t* p_data_slice = m_data_slice.data();
    uint64_t* p_data_idx = m_data_idx.data();
    CompressInfo* p_tail_info = &m_tail_info;
    CompressInfo* p_head_info = &m_head_info;
    uint16_t* dst_tail = dst_decode->dst_tail;
    int16_t* dst_head = dst_decode->dst_head;
    uint8_t* dst_bit = dst_decode->bit;

    for (int i = 0; i < data_count; i++)
    {
        std::iota(p_data_idx, p_data_idx + 2 * beam_count, 2 * i * beam_count);
        memcpy(p_data_slice, &src_data[p_data_idx[0]], 2 * beam_count);
        
        #pragma omp parallel num_threads(num_core)
        {
            #pragma omp for schedule(static)
            for (int j = 0; j < 2 * beam_count; j = j + 2)
            {
                uint8_t tail_bit = DecodeWorkers7Bit(j, p_data_slice, p_data_idx, p_tail_info, dst_tail); // mag
                uint8_t head_bit = DecodeWorkers7Bit(j + 1, p_data_slice, p_data_idx, p_head_info, dst_head); // phase

                FillBitFlags(i, j, beam_count, head_bit, tail_bit, dst_bit);
                
            }
        }
    }
}

inline void ADPCM::DecodeCompressedBlock(
    int beam_count, 
    int data_count, 
    CompressBit compr_bit,
    uint8_t* __restrict src_data, 
    Decoded* __restrict dst_decode,
    int num_core)
{

    if (compr_bit == CompressBit::Bit4)
    {
        Decode4BitCompressed(beam_count, data_count, src_data, dst_decode);
    }
    else if (compr_bit == CompressBit::Bit7)
    {
        Decode7BitCompressed(beam_count, data_count, src_data, dst_decode, num_core);
    }
    else
    {
        // error
    }

}
