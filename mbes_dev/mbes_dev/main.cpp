#include "stdafx.h"

int main()
{
    std::vector<uint8_t> data;
    BinaryReader(data, "./mat2vs_data/compr_dlength_1884160_bcnt_512_dcnt_1840.bin");

    CompressBit compr_bit = CompressBit::Bit7;

    int beam_count = 512;
    int data_count = 1840;
    int max_beam_count = 1024;
    int max_data_count = 6900;
    ADPCM adpcm(max_beam_count, max_data_count);

    unsigned long long ping_length;

    if (compr_bit == CompressBit::Bit4)
        ping_length = data.size() / (beam_count * data_count);
    else if (compr_bit == CompressBit::Bit7)
        ping_length = data.size() / (beam_count * data_count) / 2;

    std::vector<uint16_t> dst_mag(beam_count * data_count * ping_length);
    std::vector<int16_t> dst_phase(beam_count * data_count * ping_length);
    std::vector<uint8_t> dst_bit(beam_count * data_count * ping_length);

    std::chrono::system_clock::time_point start = std::chrono::system_clock::now();

    int num_core = 4;

    int loop_cnt = 500;
    ////////////////////////////////////////////////////////////////////////////////////////
    ///
    Decoded dst;

    for (int j = 0; j < loop_cnt; j++)
        for (int i = 0; i < ping_length; i++)
        {
            dst.dst_head = dst_phase.data() + i * (2 * beam_count * data_count);
            dst.dst_tail = dst_mag.data() + i * (2 * beam_count * data_count);
            dst.bit = dst_bit.data() + i * (2 * beam_count * data_count);
            //adpcm.DecodeCompressedBlock(beam_count, data_count,
            //    data.data() + i * (beam_count * data_count),
            //    dst_mag.data() + i * (beam_count * data_count),
            //    dst_phase.data() + i * (beam_count * data_count));
            adpcm.DecodeCompressedBlock(beam_count, data_count, compr_bit, data.data(), &dst, num_core);

        }

    std::chrono::duration<double> sec = std::chrono::system_clock::now() - start;

    BinaryWriter(dst_mag, "C:/����2�� ������/WorkSpace_Git/MATLAB/mbes/multiple_subarray/vs2mat/cppMag.bin");
    BinaryWriter(dst_phase, "C:/����2�� ������/WorkSpace_Git/MATLAB/mbes/multiple_subarray/vs2mat/cppPhase.bin");
    BinaryWriter(dst_bit, "C:/����2�� ������/WorkSpace_Git/MATLAB/mbes/multiple_subarray/vs2mat/cppBit.bin");


    return 0;
}